﻿namespace RoundRobinGame
{
    partial class IndexForm
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IndexForm));
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.listViewPlayers = new System.Windows.Forms.ListView();
            this.labelTimeGame = new System.Windows.Forms.Label();
            this.labelScoreGame = new System.Windows.Forms.Label();
            this.labelTime = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelScore = new System.Windows.Forms.Label();
            this.labelFlyingObject = new System.Windows.Forms.Label();
            this.buttonPlayGame = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonPlayGame)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox2.Location = new System.Drawing.Point(159, 449);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(141, 87);
            this.pictureBox2.TabIndex = 20;
            this.pictureBox2.TabStop = false;
            // 
            // listViewPlayers
            // 
            this.listViewPlayers.HideSelection = false;
            this.listViewPlayers.Location = new System.Drawing.Point(84, 165);
            this.listViewPlayers.Name = "listViewPlayers";
            this.listViewPlayers.Size = new System.Drawing.Size(244, 259);
            this.listViewPlayers.TabIndex = 19;
            this.listViewPlayers.UseCompatibleStateImageBehavior = false;
            this.listViewPlayers.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ListViewPlayers_MouseClick_1);
            // 
            // labelTimeGame
            // 
            this.labelTimeGame.AutoSize = true;
            this.labelTimeGame.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.labelTimeGame.ForeColor = System.Drawing.SystemColors.Control;
            this.labelTimeGame.Location = new System.Drawing.Point(185, -34);
            this.labelTimeGame.Name = "labelTimeGame";
            this.labelTimeGame.Size = new System.Drawing.Size(20, 24);
            this.labelTimeGame.TabIndex = 13;
            this.labelTimeGame.Text = "0";
            // 
            // labelScoreGame
            // 
            this.labelScoreGame.AutoSize = true;
            this.labelScoreGame.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.labelScoreGame.ForeColor = System.Drawing.SystemColors.Control;
            this.labelScoreGame.Location = new System.Drawing.Point(137, 65);
            this.labelScoreGame.Name = "labelScoreGame";
            this.labelScoreGame.Size = new System.Drawing.Size(20, 24);
            this.labelScoreGame.TabIndex = 15;
            this.labelScoreGame.Text = "0";
            // 
            // labelTime
            // 
            this.labelTime.AutoSize = true;
            this.labelTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTime.ForeColor = System.Drawing.SystemColors.Control;
            this.labelTime.Location = new System.Drawing.Point(80, -34);
            this.labelTime.Name = "labelTime";
            this.labelTime.Size = new System.Drawing.Size(81, 24);
            this.labelTime.TabIndex = 12;
            this.labelTime.Text = "Tiempo";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Control;
            this.label1.Location = new System.Drawing.Point(159, 669);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 24);
            this.label1.TabIndex = 18;
            this.label1.Text = "Jugar";
            // 
            // labelScore
            // 
            this.labelScore.AutoSize = true;
            this.labelScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelScore.ForeColor = System.Drawing.SystemColors.Control;
            this.labelScore.Location = new System.Drawing.Point(42, 65);
            this.labelScore.Name = "labelScore";
            this.labelScore.Size = new System.Drawing.Size(80, 24);
            this.labelScore.TabIndex = 14;
            this.labelScore.Text = "Puntaje";
            // 
            // labelFlyingObject
            // 
            this.labelFlyingObject.AutoSize = true;
            this.labelFlyingObject.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFlyingObject.ForeColor = System.Drawing.SystemColors.Control;
            this.labelFlyingObject.Location = new System.Drawing.Point(121, 131);
            this.labelFlyingObject.Name = "labelFlyingObject";
            this.labelFlyingObject.Size = new System.Drawing.Size(163, 20);
            this.labelFlyingObject.TabIndex = 16;
            this.labelFlyingObject.Text = "Esoger objeto volador";
            // 
            // buttonPlayGame
            // 
            this.buttonPlayGame.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonPlayGame.BackgroundImage")));
            this.buttonPlayGame.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonPlayGame.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonPlayGame.Location = new System.Drawing.Point(141, 558);
            this.buttonPlayGame.Name = "buttonPlayGame";
            this.buttonPlayGame.Size = new System.Drawing.Size(105, 91);
            this.buttonPlayGame.TabIndex = 17;
            this.buttonPlayGame.TabStop = false;
            this.buttonPlayGame.Click += new System.EventHandler(this.ButtonPlayGame_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.label2.ForeColor = System.Drawing.SystemColors.Control;
            this.label2.Location = new System.Drawing.Point(330, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 24);
            this.label2.TabIndex = 22;
            this.label2.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.Control;
            this.label3.Location = new System.Drawing.Point(219, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 24);
            this.label3.TabIndex = 21;
            this.label3.Text = "Tiempo";
            // 
            // IndexForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(395, 720);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.listViewPlayers);
            this.Controls.Add(this.labelTimeGame);
            this.Controls.Add(this.labelScoreGame);
            this.Controls.Add(this.labelTime);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelScore);
            this.Controls.Add(this.labelFlyingObject);
            this.Controls.Add(this.buttonPlayGame);
            this.Name = "IndexForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.IndexForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonPlayGame)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.ListView listViewPlayers;
        private System.Windows.Forms.Label labelTimeGame;
        private System.Windows.Forms.Label labelScoreGame;
        private System.Windows.Forms.Label labelTime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelScore;
        private System.Windows.Forms.Label labelFlyingObject;
        private System.Windows.Forms.PictureBox buttonPlayGame;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}

