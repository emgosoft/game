﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RoundRobinGame
{
    public partial class IndexForm : Form
    {
        Game viewGame;
        string pathDirection = Directory.GetCurrentDirectory() + "\\images\\" + "players\\";
        String[] paths = { };
        string pathImagePlayer;
        public IndexForm()
        {
            InitializeComponent();
        }

        private void IndexForm_Load(object sender, EventArgs e)
        {
            listViewPlayers.View = View.Details;
            listViewPlayers.Columns.Add("Spacecraft", 150);
            listViewPlayers.AutoResizeColumn(0, ColumnHeaderAutoResizeStyle.HeaderSize);
            addImageToListView();
        }

        private void addImageToListView()
        {
            ImageList imgs = new ImageList();
            imgs.ImageSize = new Size(50, 50);

            paths = Directory.GetFiles(pathDirection);
            try
            {
                foreach (String path in paths)
                {
                    Console.WriteLine(path);
                    imgs.Images.Add(Image.FromFile(path));
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

            listViewPlayers.SmallImageList = imgs;
            for (int i = 0; i < paths.Length; i++)
            {
                listViewPlayers.Items.Add(getNameImg(paths[i]), i);
            }
        }

        private string getNameImg(string name)
        {
            int position = name.LastIndexOf('\\') + 1;
            int nameSize = name.Length - position;
            name = name.Substring(position, nameSize);
            return name;
        }

        private void ListViewPlayers_MouseClick_1(object sender, MouseEventArgs e)
        {
            string pathImage = paths[listViewPlayers.FocusedItem.Index];
            pathImagePlayer = pathImage;
            pictureBox2.Image = Image.FromFile(pathImage);
            pictureBox2.Refresh();
            pictureBox2.Update();
        }

        private void ButtonPlayGame_Click(object sender, EventArgs e)
        {
            if(!string.IsNullOrEmpty(pathImagePlayer))
            {
                viewGame = new Game(pathImagePlayer);
                viewGame.Visible = true;
            } else
            {
                MessageBox.Show("Necesitas ingresar un objeto volador para jugar");
            }
        }
    }
}
