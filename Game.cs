﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RoundRobinGame
{
    public partial class Game : Form
    {
        Coin coin;
        AdministratorBlock blocks;
        string playerObjetc;
        enum Position
        {
            Left, Right, Up, Down
        }

        private int _x;
        private int _y;
        private Position _objPosition;

        public int formHeigh;
        public int formWidth;
        int timer = 0;
        int positionBlocking;
        int totalCoins = 4;
        string pathDirectionProcess = Directory.GetCurrentDirectory() + "\\images\\" + "procesos\\";

        //List<string> archivos = new List<string>();

        public Game()
        {
            InitializeComponent();
            _x = 50;
            _y = 50;
            formHeigh = this.Height;
            formWidth = this.Width;
            _objPosition = Position.Down;
            blocks = new AdministratorBlock(formWidth, formHeigh);
            Console.WriteLine("Administrador creado");
        }

        public Game(string pathImage)
        {
            Console.WriteLine(pathImage);
            InitializeComponent();
            _x = 50;
            _y = 50;
            formHeigh = this.Height;
            formWidth = this.Width;
            _objPosition = Position.Down;
            playerObjetc = pathImage;
            blocks = new AdministratorBlock(formWidth, formHeigh);
        }

        private void Game_Load(object sender, EventArgs e)
        {
            var thMovePlayer = new Thread(movePlayer);
            thMovePlayer.Start();
            var thBlocking = new Thread(moveBlocking);
            //th1.Start();
            // var thBonus = new Thread();
        }
        private void Game_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(new Bitmap(playerObjetc), _x, _y, 64, 64);
            blocks.paintBlocks(e);
        }

        private void Game_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left)
            {
                _objPosition = Position.Left;
            }
            else if (e.KeyCode == Keys.Right)
            {
                _objPosition = Position.Right;
            }
            else if (e.KeyCode == Keys.Up)
            {
                _objPosition = Position.Up;
            }
            else if (e.KeyCode == Keys.Down)
            {
                _objPosition = Position.Down;
            }
        }

        public void movePlayer()
        {
            while (true)
            {
                try
                {
                    if (_objPosition == Position.Right)
                    {
                        _x += 10;
                    }
                    else if (_objPosition == Position.Left)
                    {
                        _x -= 10;
                    }
                    else if (_objPosition == Position.Up)
                    {
                        _y -= 10;
                    }
                    else if (_objPosition == Position.Down)
                    {
                        _y += 10;
                    }

                    Invalidate();

                    Thread.Sleep(100);
                }
                catch (Exception e)
                {
                    ;
                }
            }
        }

        public void moveBlocking()
        {
            while (true)
            {
                try
                {
                    Console.WriteLine("hola prro");
                    Thread.Sleep(1000);
                } catch(Exception e)
                {
                    ;
                }
            }
        }

        public void generateBonus()
        {
            Random random = new Random();
            for (int i = 1; i < totalCoins; i++)
            {
                coin = new Coin();
                coin.xPosition = generateRadomPosition();
                coin.yPosition = generateRadomPosition();
                coin.value = random.Next(1, 100);
                coin.image = pathDirectionProcess + "bonus.png";
            }
        }

        public int generateRadomPosition()
        {
            Random random = new Random();
            return random.Next(formWidth, formHeigh);
        }
    }
}
