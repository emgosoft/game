﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundRobinGame
{
    class Coin
    {
        public int xPosition { set; get; }
        public int yPosition { set; get; }
        public int time { set; get; }
        public int value { set; get; }
        public string image { set; get; }

    }
}
