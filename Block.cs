﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace RoundRobinGame
{
    class Block
    {
        int xStart;
        int yStart;
        int x;
        int y;
        string path;
        Random random;
        
        public Block(int x, int y, string path, Random random)
        {
            this.random = random;
            this.x = xStart = x;
            yStart = y;
            this.y = random.Next(y - 100);
            this.path = path;
        }
        public void moveBlock()
        {
            x-= 50;
            if (x < -40)
            {
                x = xStart;
                y = random.Next(yStart - 70);
            }
        }

        public int PropertyX
        {
            get
            {
                return PropertyX;
            }
            set
            {
                PropertyX = value;
            }
        }

        public int PropertyY
        {
            get
            {
                return PropertyY;
            }
            set
            {
                PropertyY = value;
            }
        }

        public void draw(PaintEventArgs e)
        {
            Console.WriteLine("(" + x + ", " + " " + y + ")");
            Bitmap img;
            img = new Bitmap(path);

            try
            {
                e.Graphics.DrawImage(img, x, y, 64, 64);
            }
            catch (Exception)
            {
                ;
            }
        }
    }
}
