﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RoundRobinGame
{
    class AdministratorBlock
    {
        static string pathDirection = Directory.GetCurrentDirectory() + "\\images\\" + "procesos\\";
        const int quantum = 2;
        static int count = 0;
        static int indexBlock = 0;
        static List<Block> blocks = new List<Block>();
        public AdministratorBlock(int widht, int height)
        {
            Random rnd = new Random();
            Block block;

            block = new Block(widht - 40, height, pathDirection + "bomb.png", rnd);
            blocks.Add(block);
            block = new Block(widht - 40, height, pathDirection + "bullet.png", rnd);
            blocks.Add(block);
            block = new Block(widht - 40, height, pathDirection + "misil.png", rnd);
            blocks.Add(block);
            block = new Block(widht - 40, height, pathDirection + "torpedo.png", rnd);
            blocks.Add(block);

            Thread  t = new Thread(MoveBlocks);
            t.Start();
            //t.Join(); //wait for thread t
            Console.WriteLine("Created thread finished");
        }

        static void MoveBlocks()
        {
            while(true){
                try
                {
                    updatePositionBlock();
                    Thread.Sleep(200);
                }
                catch (Exception e)
                {
                    ;
                }
            }
        }

        private static void updatePositionBlock()
        {
            count++;
            blocks[indexBlock].moveBlock();
            if (count >= quantum)
            {
                count = 0;
                indexBlock++;
                if (indexBlock >= blocks.Count())
                {
                    indexBlock = 0;
                }
            }

        }

        public void paintBlocks(PaintEventArgs e)
        {
            for(int i = 0; i < blocks.Count(); i++)
            {
                blocks[i].draw(e);
            }
        }
    }
}
